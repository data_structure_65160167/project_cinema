/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectcinema;

import static com.mycompany.projectcinema.Movie.number;
import java.util.Scanner;

/**
 *
 * @author uSeR
 */
public class Showtime {
    static String[] date = {"28/08/2023", "29/08/2023"};
    static String[] time = {"10.00(TH)", "14.00(ENG)"};
    static String day;
    static String round;
    static Scanner kb = new Scanner(System.in);
    public static void ShowtDate() {
        for (int i = 0; i < date.length; i++) {
            System.out.println(date[i] + "");
        }
    }
    public static void selectDate() {
        while (true) {
            ShowtDate();
            System.out.print("Please Choose your date :");
            day = kb.next();
            if(day.equals("28/08/2023")){
                SelectTime();
                break;
            }else if(day.equals("29/08/2023")){    
                SelectTime();
                break;
            }else{
                ShowtDate();
                System.out.println("Please Choose your date again.");
                continue;
            }
        }
    }
    public static void ShowTime() {
        for (int i = 0; i < time.length; i++) {
            System.out.println(time[i] + "");
        }
    }
    public static void SelectTime() {
        while (true) {
            ShowTime();
            System.out.print("Please Choose your time :");
            round = kb.next();
            if(round.equals("10.00(TH)")){
                Ticket.TimeBooking.add(time[0]);
                if(day.equals("28/08/2023")){
                    Ticket.DateBooking.add(date[0]);
                    if(number == 1){
                        Seat.BarbieSeats_1_1();
                        Seat.selectSeat();
                    }else if(number == 2){
                        Seat.TheMeg2Seats_1_1();
                        Seat.selectSeat();
                    }else if(number == 3){
                        Seat.LongLiveLoveSeats_1_1();
                        Seat.selectSeat();
                    }
                }else if(day.equals("29/08/2023")){
                    Ticket.DateBooking.add(date[1]);
                    if(number == 1){
                        Seat.BarbieSeats_2_1();
                        Seat.selectSeat();
                    }else if(number == 2){
                        Seat.TheMeg2Seats_2_1();
                        Seat.selectSeat();
                    }else if(number == 3){
                        Seat.LongLiveLoveSeats_2_1();
                        Seat.selectSeat();
                    }
                }
                break;
            }else if(round.equals("14.00(ENG)")){
                Ticket.TimeBooking.add(time[1]);
                if(day.equals("28/08/2023")){
                    Ticket.DateBooking.add(date[0]);
                    if(number == 1){
                        Seat.BarbieSeats_1_2();
                        Seat.selectSeat();
                    }else if(number == 2){
                        Seat.TheMeg2Seats_1_2();
                        Seat.selectSeat();
                    }else if(number == 3){
                        Seat.LongLiveLoveSeats_1_2();
                        Seat.selectSeat();
                    }
                }else if(day.equals("29/08/2023")){
                    Ticket.DateBooking.add(date[1]);
                    if(number == 1){
                        Seat.BarbieSeats_2_2();
                        Seat.selectSeat();
                    }else if(number == 2){
                        Seat.TheMeg2Seats_2_2();
                        Seat.selectSeat();
                    }else if(number == 3){
                        Seat.LongLiveLoveSeats_2_2();
                        Seat.selectSeat();
                    }
                }
                break;
  
            }else{
                ShowtDate();
                System.out.println("Please Choose your time again.");
                continue;
            }
        }
    }
}
